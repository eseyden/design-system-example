import React from "react";
import styled from "styled-components";
import { useSpring, animated, config } from "react-spring";
import { typeScale } from "../utils";
import { CloseIcon, Illustrations } from "../assets";
import { PrimaryButton } from "./Buttons";

const ModalWrapper = styled.div`
    width: 900px;
    height: 600px;
    box-shadow: 0 5px 16px rgba(0,0,0, 0.2);
    background-color: ${props => props.theme.formElementBackground};
    color: ${props => props.theme.textOnFormElementBackground};
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
    border-radius: 2px;
`;

const SignUpHeader = styled.h3`
    font-size: ${typeScale.header3};
    color: ${props => props.theme.textColor }
`;

const SignUpText = styled.p`
    font-size: ${typeScale.paragraph};
    max-width: 70%large;
    text-align: center;
    color: ${props => props.theme.textColor }
`;

const CloseModalButton = styled.button`
    cursor: pointer;
    background: none;
    border: none;
    position: absolute;
    right: 40px;
    top: 40px;
    height: 24px;
    width: 24px;
    padding: 0;
    fill: ${props => props.theme.textColor }
`;



export const SignUpModal = ({ showModal, setShowModal }) => {
    const animation = useSpring({ 
        opacity: showModal ? 1 : 0,
        transform: showModal ? `translateY(0)`: `translate(-200%)`,
        config: config.slow
    });
    return (
        <animated.div style={animation}>
            <ModalWrapper>
                <img src={Illustrations.SignUp} alt="Sign Up" aria-hidden="true"  />
                <SignUpHeader>Sign Up</SignUpHeader>
                <SignUpText>Sign up today to get  access!</SignUpText>
                <PrimaryButton>Sign up!</PrimaryButton>
                <CloseModalButton aria-label="Close Modal">
                    <CloseIcon />
                </CloseModalButton>
            </ModalWrapper>
        </animated.div>
    )
}