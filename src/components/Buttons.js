import styled from "styled-components";
import { typeScale } from "../utils";
import { applyStyleModifiers } from "styled-components-modifiers";

const BUTTON_MODIFIERS = {
    small: () => `
        font-size: ${typeScale.helperText};
        padding: 8px;
    `,
    large: () => `
        font-size: ${typeScale.header5};
        padding: 16px 24px;
    `,
    warning: ({ theme }) => `
        background: none;
        color: ${ theme.status.warningColor };
        
        &:hover, &:focus {
            background-color: ${theme.status.warningColorHover};
        }

        &:focus {
            outline: 2px solid ${theme.status.warningColorHover};
            outline-offset: 2px;
        }

        &:active {
        background-color: ${theme.status.warningColorActive};
        }
    `,
    error: ({ theme }) => `
        background: none;
        color: ${theme.status.errorColor};
        &:hover, &:focus {
            background-color: ${theme.status.errorColorHover};
        }
        &:focus{

        }
        &:active {
            background-color: ${theme.status.errorColorActive};
        }
    `,

    success: ({ theme }) => `
        background: none;
        color: ${theme.status.successColor};
        
        &:hover, &:focus {
            background-color: ${theme.status.successColorHover};
        }

        &:focus {
            outline: 2px solid ${theme.status.successColorHover};
            outline-offset: 2px;
        }

        &:active {
            background-color: ${theme.status.successColorActive};
        }
    `
}

const PRIMARY_MODIFIERS = {
    warning: ({ theme }) => `
        background-color: ${theme.status.warningColor};
        color: ${theme.textColorInverted};
    `,
    error: ({ theme }) => `
        background-color: ${theme.status.errorColor};
        color: ${theme.textColorInverted};
    `,
    success: ({ theme }) => `
        background-color: ${theme.status.successColor};
        color: ${theme.textColorInverted};
    `
}

const SECONDARY_MODIFIERS = {
    warning: ({ theme }) => `
        border: 2px solid ${theme.status.warningColor};
        &:focus {
            border: 2px solid ${theme.status.warningColor};
        }
    `,
    error: ({ theme }) => `
        border: 2px solid ${theme.status.errorColor};
        &:focus {
            border: 2px solid ${theme.status.errorColor};
        }
    `,
    success: ({ theme }) => `
        border: 2px solid ${theme.status.successColor};
        &:focus {
            border: 2px solid ${theme.status.successColor};
        }
    `
}

const TERTIARY_MODIFIERS = {
    warning: () => `
    `
}

const Button = styled.button`
    margin: 5px;
    padding: 12px 24px;
    font-size: ${typeScale.paragraph};
    border-radius: 2px;
    min-width: 100px;
    cursor: pointer;
    font-family: Arial,Helvetica,san-serif;
    text-transform: uppercase;
    letter-spacing: 2px;
    font-weight: 900;
    transition: background-color 0.2s linear, color 0.2s linear;

    &:hover {
        background-color: ${props => props.theme.primaryColorHover};
        color: ${props => props.theme.textColorOnPrimary};
    }
    &:focus {
        background-color: ${props => props.theme.primaryColorHover};
        color: ${props => props.theme.textColorOnPrimary};
        outline: 2px solid ${props => props.theme.primaryOutline};
        outline-offset: 2px;
    }
    &:disabled {
        cursor: not-allowed;
    }
`;

export const PrimaryButton = styled(Button)`
    border: none;
    background-color: ${props => props.theme.primaryColor};
    color: ${props => props.theme.textColorOnPrimary};
    &:disabled {
        background-color: ${props => props.theme.disabled}
    }
    ${applyStyleModifiers(BUTTON_MODIFIERS)}
    ${applyStyleModifiers(PRIMARY_MODIFIERS)}
`;

export const SecondaryButton = styled(Button)`
    background: none;
    border: 2px solid ${props => props.theme.primaryColor};
    color: ${props => props.theme.primaryColor};
    &:focus {
        border: 2px solid ${props => props.theme.primaryColorHover};
    }
    &:disabled {
        background: none;
        color: ${props => props.theme.disabled};
        border-color: ${props => props.theme.disabled};
    }
    ${applyStyleModifiers(BUTTON_MODIFIERS)}
    ${applyStyleModifiers(SECONDARY_MODIFIERS)}
`;

export const TertiaryButton = styled(Button)`
    background: none;
    border: none;
    color: ${props => props.theme.primaryColor};

    &:disabled {
        background: none;
        color: ${props => props.theme.disabled};
    }
    ${applyStyleModifiers(BUTTON_MODIFIERS)}
    ${applyStyleModifiers(TERTIARY_MODIFIERS)}
`;

export const ButtonsDemo = (props) => {
    let useDarkTheme = false;
    if(props.useDarkTheme !== undefined)
    {
        useDarkTheme = props.useDarkTheme;
    }
    return (
        <div
            style={{
                background: useDarkTheme
                    ? 'grey'
                    : 'white',
                width: "100vw",
                height: "100px",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around"
            }}
        >
            <PrimaryButton>Hello world</PrimaryButton>
            <SecondaryButton>Hi there</SecondaryButton>
            <TertiaryButton>Hello There</TertiaryButton>

            <PrimaryButton disabled >Hello world</PrimaryButton>
            <SecondaryButton disabled >Hi there</SecondaryButton>
            <TertiaryButton disabled >Hello There</TertiaryButton>
            <br />
            <PrimaryButton modifiers="warning">Hello world</PrimaryButton>
            <SecondaryButton modifiers="warning">Hi there</SecondaryButton>
            <TertiaryButton modifiers="warning">Hello There</TertiaryButton>
            <br />
            <PrimaryButton modifiers="success">Hello world</PrimaryButton>
            <SecondaryButton modifiers="success">Hi there</SecondaryButton>
            <TertiaryButton modifiers="success">Hello There</TertiaryButton>
            <br />
            <PrimaryButton modifiers="error">Hello world</PrimaryButton>
            <SecondaryButton modifiers="error">Hi there</SecondaryButton>
            <TertiaryButton modifiers="error">Hello There</TertiaryButton>
        </div>
    );
}

export const BasicButton = styled.button`
    margin: 0 16px 24px;
    padding: 8px;
    background: none;
    cursor: pointer;
    border: 2px solid #000;
    margin-top: 60px;
`;