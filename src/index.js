import React, { useState } from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "styled-components";
import { SignUpModal, BasicButton } from "./components";
import { GlobalStyle, defaultTheme, darkTheme } from "./utils";

const App = () => {
    const [useDarkTheme, setUseDarkTheme] = useState(false);
    const [showModal, setShowModal] = useState(false);

    return (
        <ThemeProvider theme={useDarkTheme ? darkTheme: defaultTheme}>
            <BasicButton onClick={() => setUseDarkTheme(true)}>
                Dark theme
            </BasicButton>
            <BasicButton onClick={() => setUseDarkTheme(false)}>
                Default theme
            </BasicButton>
            <BasicButton onClick={() => setShowModal(!showModal)} >Show Modal</BasicButton>
            <SignUpModal showModal={showModal} setShowModal={setShowModal} />
            <GlobalStyle />
        </ThemeProvider>
    );
}

ReactDOM.render(<App />, document.querySelector('#root'));
