import { addons } from "@storybook/addons"
import { myTheme } from './myTheme'
addons.setConfig({
    themes: myTheme
})